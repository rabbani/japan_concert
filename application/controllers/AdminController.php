<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('userid')){
            redirect('welcome');
        }
    }

    /**
     * for show admin dashboard
     */
    public function index()
    {
        $this->load->view('dashboard/header');
        $this->load->view('dashboard/sidebar');
        $this->load->view('dashboard/index');
        $this->load->view('dashboard/footer');
    }






    public function logout()
    {

        $this->session->sess_destroy();
        $this->session->set_flashdata('success', 'You are logout successfully!');
        redirect('admin');
    }

}

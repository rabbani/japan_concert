<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TicketController extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
//        if (!$this->session->userdata('userid')) {
//            redirect('welcome');
//        }
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {

    }


    /*================================
     * for store ticket information
     * ===============================
     * */
    public function store()
    {
        


        $name = $this->input->post('name');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $address = $this->input->post('address');
        $note = $this->input->post('note');


        $payment_method = $this->input->post('payment_method');
        $sub_total_amount = $this->input->post('sub_total_amount');
        $sub_total_seat = $this->input->post('sub_total_seat');


        $seates = $this->input->post('seat_required');

        $seat_type = $this->input->post('seat_type');
        $seat_requires = $this->input->post('seat_required');

        $participant_data = array(
            'participant_name' => $name,
            'phone_number' => $phone,
            'email' => $email,
            'address' => $address,
            'note' => $note,
            'create_date' => date('Y-m-d')

        );

        $participant_id = $this->TicketModel->store_participant($participant_data);

        $booking_info = [
            'participant_id' => $participant_id,
            'booking_date' => date('Y-m-d'),
            'subtotal_seat' => $sub_total_seat,
            'subtotal_amount' => $sub_total_amount,
            'payment_method_id' => 0
        ];
        $booking_id = $this->TicketModel->booking_info($booking_info);

        for ($i = 0; $i < count($seat_requires); $i++) {
            if ($seat_requires[$i] != 0) {
                $booking_details = [
                    'booking_id' => $booking_id,
                    'seat_type_id' => @$seat_type[$i],
                    'booking_seat_qty' => @$seat_requires[$i]
                ];
                $this->TicketModel->booking_details($booking_details);
            }
        }


        $this->TicketModel->deduction_ticket_from_db($seates);

        $current_slip_number = $this->db->select('slip_number')
            ->from('payment_slip')
            ->order_by('booking_id', 'desc')
            ->limit(1)
            ->get()
            ->row();


        if (empty($current_slip_number)) {
            $current_slip_number = 1001;
        } else {
            $current_slip_number = $current_slip_number->slip_number;
            $current_slip_number++;
        }
//        $pdf_data = [
//            'participant_name' => $name,
//            'phone_number' => $phone,
//            'email' => $email,
//            'address' => $address,
//            'note' => $note,
//            'create_date' => date('Y-m-d'),
//            'subtotal_seat' => $sub_total_seat,
//            'subtotal_amount' => $sub_total_amount,
//            'slip_number' => $current_slip_number
//
//        ];
        $slip_data = [
            'booking_id' => $booking_id,
            'file_name' => 'null',
            'slip_number' => $current_slip_number,
            'submit_date' => date('Y-m-d')
        ];

        $this->TicketModel->insert_payment_slip($slip_data);



        $this->session->set_flashdata('message', '<div class="alert alert-success">Thank you </div>');
        redirect('pdf/' . $participant_id);
    }

    public function pdf($id)
    {
        $participants = $this->db->select('*')
            ->from('participants')
            ->where('participant_id', $id)
            ->get()
            ->row();

        $booking_info = $this->db->select('*')
            ->from('booking_info')
            ->where('participant_id', $id)
            ->get()
            ->row();
        $booking_id = $booking_info->booking_id;

        $payment_slip = $this->db->select('*')
            ->from('payment_slip')
            ->where('booking_id', $booking_id)
            ->get()
            ->row();
        $data=[
            'participants'=>$participants,
            'booking_info'=>$booking_info,
            'payment_slip'=>$payment_slip
        ];

//        echo "<pre>";
//        print_r($data);
//        exit();
        $this->load->view('pdfreport', $data);
    }


    //    ============ its for upload_pdf_file ============
    public function upload_pdf_file() {
        $slip_number = $this->input->post('slip_number');

        $config = array(
            'upload_path' => "./uploads/slips/",
            'allowed_types' => "gif|jpg|png|jpeg|pdf",
            'overwrite' => TRUE,
            'file_name' => "BDTASK" . time(),
            'max_size' => '0',
        );
//        echo '<pre>';        print_r($config);die();
        $image_data = array();
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if ($this->upload->do_upload('file_name')) {
//            echo 'Inside';die();
            $image_data = $this->upload->data();
//                print_r($image_data); die();
            $image_name = $image_data['file_name'];

        }
//        echo 'outside';die();

        $allinfo = array(
            'file_name' => $image_name,
            'slip_number' => $slip_number,
        );
        $this->db->where('slip_number', $slip_number);
        $this->db->update('payment_slip', $allinfo);
        $this->session->set_flashdata('message', '<div class="alert alert-success">Thank you, File uploaded successfully! </div>');
        redirect('welcome');
    }
//    =======slip_list ============
    public function slip_list(){
        $data['slip_lists'] = $this->TicketModel->slip_lists();
        $this->load->view('dashboard/header');
        $this->load->view('dashboard/sidebar');
        $this->load->view('dashboard/slip_lists', $data);
        $this->load->view('dashboard/footer');
    }
    //================show all Participant==============

    public function participant_list()
    {
        $data['participant_lists'] = $this->TicketModel->participant_list();
        $this->load->view('dashboard/header');
        $this->load->view('dashboard/sidebar');
        $this->load->view('dashboard/participant_list', $data);
        $this->load->view('dashboard/footer');
    }
}

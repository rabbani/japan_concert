<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /*================================
    show front end home
    ==================================
    */
    public function index()
    {
        $data['available_seats'] = $this->TicketModel->available_seat();
        $this->load->view('index', $data);
    }

    //show login form
    public function login_form()
    {
        $this->load->view('login');
    }


    //check use exists or not
    public function login()
    {
        $name = $this->input->post('username');
        $password = $this->input->post('password');

        $query = $this->db->select('*')
            ->from('users')
            ->where('name', $name)
            ->where('password', md5($password))
            ->get();
        $users = $query->result();
        if ($users) {
            $this->session->set_userdata('userid', $users[0]->id);
            $this->session->set_userdata('username', $users[0]->name);
            redirect('dashboard');
        }else{
            redirect('admin');
        }

    }


}

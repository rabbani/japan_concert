<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TicketModel extends CI_Model
{


    public function available_seat()
    {
        $query = $this->db->select('*')
            ->from('seat_type')
            ->join('available_seats', 'seat_type.seat_type_id  = available_seats.seat_type_id')
            ->get();
        return $query->result();
    }

    //store participant information
    public function store_participant($participant_data)
    {
        $this->db->insert('participants', $participant_data);
        return $participants_id = $this->db->insert_id();
    }

    //store booking details data
    public function booking_details($booking_details)
    {
        $this->db->insert('booking_details', $booking_details);

    }


    //store booking information data
    public function booking_info($booking_info)
    {
        $this->db->insert('booking_info', $booking_info);
        return $booking_id = $this->db->insert_id();
    }


    //after purchase ticket subtract ticket
    public function deduction_ticket_from_db($seates)    {

        $query = $this->db->select('*')
            ->from('available_seats')
            ->get();

        $total_seates = $query->result();

        for ($i = 0, $j = 0; $i < count($total_seates); $i++, $j++) {
            $remain_seats= $total_seates[$i]->seats_available - $seates[$j];
            $this->db->set('seats_available',$remain_seats)
                ->where('id', $total_seates[$i]->id)
                ->update('available_seats');

        }
    }

    public function insert_payment_slip($slip_data)
    {
        $this->db->insert('payment_slip',$slip_data);

    }

    //show all participant data
    public function participant_list()
    {
        $query = $this->db->select('*')
            ->from('participants')
            ->join('booking_info', 'participants.participant_id  = booking_info.participant_id')
            ->get();
        return $query->result();
    }


//    =========== slip_lists ==========
    public function slip_lists(){
        $query = $this->db->select('*')
            ->from('payment_slip')
            ->get();
        return $query->result();
    }


}


            <!-- =============================================== -->
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <div class="header-icon">
                        <i class="pe-7s-world"></i>
                    </div>
                    <div class="header-title">
                        <h1>Bdtask - Bootstrap Admin Template Dashboard</h1>
                        <small>Very detailed & featured admin.</small>
                        <ol class="breadcrumb">
                            <li><a href="../index.html"><i class="pe-7s-home"></i> Home</a></li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </section>
                <!-- Main content -->
                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-bd">
                                <div class="panel-body">
                                    <h3 class="text-center">Welcome to Dashboard</h3>
                                </div>
                            </div>
                        </div>

                    </div>
                     <!-- /.row -->
                </section> <!-- /.content -->
            </div> <!-- /.content-wrapper -->

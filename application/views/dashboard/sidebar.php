<!-- =============================================== -->
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel text-center">
            <div class="image">
                <img src="<?php echo base_url('assets/dist/img/user2-160x160.png')?>" class="img-circle" alt="User Image">
            </div>
            <div class="info">
                <p>Naeem Khan</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn"><i class="fa fa-search"></i></button>
                            </span>
            </div>
        </form> <!-- /.search form -->
        <!-- sidebar menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="dashboard"><i class="ti-home"></i> <span>Dashboard</span>
                    <span class="pull-right-container">
                                    <span class="label label-success pull-right">v.1</span>
                                </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="ti-paint-bucket"></i><span>Participant </span>
                    <span class="pull-right-container">
                                    <i class="fa fa-angle-left pull-right"></i>
                                </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="participant-list">All Participant</a></li>

                    <li><a href="slip-list">Slip List</a></li>
                </ul>
            </li>



        </ul>
    </div> <!-- /.sidebar -->
</aside>
<!-- =============================================== -->
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="header-icon">
            <i class="pe-7s-world"></i>
        </div>
        <div class="header-title">
            <h1>Bdtask - Bootstrap Admin Template Dashboard</h1>
            <small>Very detailed & featured admin.</small>
            <ol class="breadcrumb">
                <li><a href="../index.html"><i class="pe-7s-home"></i> Home</a></li>
                <li class="active">Dashboard</li>
            </ol>
        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel lobidisable panel-bd">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Bordered table</h4>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTableExample2" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Booking ID</th>
                                        <th>Slip Number</th>
                                        <th>Slip View</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1 ?>
                                    <?php foreach ($slip_lists as $slip): ?>
                                        <tr>
                                            <th scope="row"><?php echo $i; ?></th>
                                            <td><?php echo $slip->booking_id; ?></td>
                                            <td><?php echo $slip->slip_number; ?></td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>uploads/slips/<?php echo $slip->file_name; ?>" target="_new" style="font-size: 25px;">
                                                    <i class="fa fa-file"></i>
                                                </a>
                                        </td>

                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- /.row -->
</section> <!-- /.content -->
</div> <!-- /.content-wrapper -->

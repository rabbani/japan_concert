<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url('assets/front_end/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/front_end/fontawesome/css/all.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/front_end/css/style.css') ?>" rel="stylesheet">
    <title>Hello, world!</title>
</head>
<body>
<div class="container content">
    <div class="row">
        <div class="pull-right ml-auto">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary mb-3 mr-3" data-toggle="modal" data-target="#exampleModal">
                Upload the payment slip
            </button>
            <!-- Modal -->

            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">File Upload</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="<?php echo base_url('upload-pdf-file'); ?>" method="post" enctype="multipart/form-data" class='form-horizontal pdf_frm'>
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-file"><span class="fileupload-new">Choose File</span>
                                                <span class="fileupload-exists">Change</span>
                                                <input  type="file" name="file_name">
                                            </span>
                                    <span class="fileupload-preview"></span>
                                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                       style="float: none">×</a>
                                </div>
                                <!-- /.End of fileupload  -->
                                <p class="text-secondary">[Note: only ".jpg, .jpeg, .png, .gif, .pdf" file format are
                                    allowed]</p>
                                <div class="row">
                                    <div class="col-md-5">
                                        <input class="form-control" id="slipNumber" name="slip_number" type="text" placeholder="Slip Number">
                                    </div>
                                </div>
                                <div class="row">
                                    <input type="submit" class="btn btn-primary pull-right pdf_upload_btn" value="Upload" style="margin: 15px;">
                                </div>
                            </form>
                        </div>
                        <!--                                <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">close</button>
                                                        </div>-->
                    </div>
                </div>
            </div>
            <!-- /.End of modal content-->
        </div>
    </div>
    <form class="row" action="<?php echo base_url('store-ticket');?>" method="post">
        <div class="col-md-6 border-right">
            <?php
            $message = $this->session->flashdata('message');
            echo $message;
            ?>
            <h3 class="title">Ask For tour ticket demand</h3>
            <div class="form-content">
                <div class="form-group row">
                    <label for="name" class="col-sm-4 col-form-label">Name<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="phone" class="col-sm-4 col-form-label">Phone Number<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="phone" id="phone">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-4 col-form-label">Email<sup>*</sup></label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="email" id="email">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-sm-4 col-form-label">Living area/Address</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="address" id="address">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="note" class="col-sm-4 col-form-label">Note</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="note" id="note">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="table-content">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead class="thead-light">
                        <tr>
                            <th>Seat Type</th>
                            <th>Seat Left</th>
                            <th width="20%">Per Seat Cost</th>
                            <th>Seat Required</th>
                            <th>Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($available_seats as $seat): ?>
                            <tr>
                                <td class="text-capitalize">
                                    <input type="hidden" name="seat_type[]" value="<?php echo $seat->id ?>">
                                    <?php echo $seat->seat_type_name ?></td>
                                <td><?php echo $seat->seats_available ?></td>
                                <td class="seat_cost"
                                    id="<?php echo $seat->seat_cost ?>"><?php echo $seat->seat_cost ?> Yens
                                </td>
                                <td>
                                    <div data-quantity="Quantity" class="quantity">
                                        <div class="input-group spinner">
                                            <input type="text" class="form-control number_of_seat"
                                                   name="seat_required[]" value="0">
                                            <div class="input-group-btn-vertical">
                                                <button class="btn btn-default" id="btn_up_<?php echo $seat->id ?>"
                                                        type="button"><i
                                                            class="fas fa-angle-up"></i></button>
                                                <button class="btn btn-default down"
                                                        id="btn_down_<?php echo $seat->id ?>" type="button"><i
                                                            class="fas fa-angle-down"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td><input type="text" class=" form-control total_amount" readonly
                                           id="<?php echo $seat->seat_type_name ?>">
                                    <!--                                    <span class="total_amount" id="-->
                                    <?php //echo $seat->seat_type_name ?><!--">0</span></td>-->
                            </tr>
                        <?php endforeach; ?>

                        <tr>
                            <td colspan="5">
                                <div class="d-flex d-flex justify-content-between  align-items-center">
                                    <div class="payment">
                                        <span>Payment Method :</span>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" name="payment_method"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline1">Bank</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="payment_method"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="customRadioInline2">Cash</label>
                                        </div>
                                    </div>
                                    <div class="total">
                                        <input type="hidden" class="sub_total_seats" name="sub_total_seat">
                                        Sub Total Seat : <span class="sub_total_seat" id="sub_total_seat">0</span><br>
                                        <input type="hidden" class="sub_total_amounts" name="sub_total_amount">
                                        Sub Total Amount : <span class="sub_total_amount"
                                                                 id="sub_total_amount">0.00</span>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-12 text-right">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </form>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo base_url('assets/front_end/js/jquery-3.3.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/front_end/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url('assets/front_end/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/front_end/js/upload.js') ?>"></script>
<script>
    $(document).ready(function () {
        // spinner js

        var sub_total = 0;
        var singleId = '';
        var sub_total_seat = 0;
        $('.spinner .btn:first-of-type').on('click', function () {
            var btn = $(this);
            var input = btn.closest('.spinner').find('input');
            if (input.attr('max') === undefined || parseInt(input.val()) < parseInt(input.attr('max'))) {
                var seat = input.val(parseInt(input.val(), 10) + 1);
                var seat_number = seat.val();
                var seat_cost = $(this).parent().parent().parent().parent().prev().html();
                var total_amount = seat_number * parseInt(seat_cost);

                var get_total_amount_tag = $(this).parent().parent().parent().parent().next().html();//get single price field tag

                var get_total_id = $(get_total_amount_tag).attr('id');//get single price field id
                $('#' + get_total_id).val(total_amount);//replace single field total price

                sub_total = 0;
                $('.total_amount').each(function () {
                    isNaN(this.value) || 0 == this.value.length || (sub_total += parseFloat(this.value))
                });

                $('.sub_total_amount').text(sub_total.toFixed(2)); //replace total price
                $('.sub_total_amounts').val(sub_total.toFixed(2)); //replace total price hidden input field

                sub_total_seat = 0;
                $('.number_of_seat').each(function () {
                    isNaN(this.value) || 0 == this.value.length || (sub_total_seat += parseFloat(this.value))
                });

                $('.sub_total_seat').text(sub_total_seat.toFixed(2)); //replace total seat
                $('.sub_total_seats').val(sub_total_seat.toFixed(2)); //replace total seat hidden input field

            } else {
                btn.next("disabled", true);
            }
        });
        $('.spinner .btn:last-of-type').on('click', function () {
            var btn = $(this);
            var input = btn.closest('.spinner').find('input');
            if (input.attr('min') === undefined || parseInt(input.val()) > parseFloat(input.attr('min'))) {
                var seat = input.val(parseInt(input.val(), 10) - 1);
                var seat_number = seat.val();
                var seat_cost = $(this).parent().parent().parent().parent().prev().html();
                var total_amount = seat_number * seat_cost;

                var get_total_amount_tag = $(this).parent().parent().parent().parent().next().html();

                var get_total_id = $(get_total_amount_tag).attr('id');
                singleId = $('#' + get_total_id).val(total_amount);

                $(singleId).each(function () {
                    isNaN(this.value) || 0 == this.value.length || (sub_total -= seat_cost)
                });
                $('.sub_total_amount').text(sub_total.toFixed(2));//replace total price
                $('.sub_total_amounts').val(sub_total.toFixed(2));//replace total pricehidden input field


                sub_total_seat = 0;
                $('.number_of_seat').each(function () {
                    isNaN(this.value) || 0 == this.value.length || (sub_total_seat += parseFloat(this.value))
                });

                $('.sub_total_seat').text(sub_total_seat.toFixed(2)); //replace total seat
                $('.sub_total_seats').val(sub_total_seat.toFixed(2)); //replace total seat hidden input field
            } else {
                btn.prev("disabled", true);
            }
        });


    });


</script>
</body>
</html>
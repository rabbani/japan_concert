<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #customers {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }

        #customers tr:nth-child(even){background-color: #f2f2f2;}

        #customers tr:hover {background-color: #ddd;}

        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }




        .slip-number{
            /*text-align: center;*/
            padding: 1em;
            background: #bbb;
            width: 200px;
            margin: 0 auto;
            border-radius: 5px 5px 0 0 ;

        }

        .payment-details table {
            padding: 2em;
            border: 1px solid #aaa;
            text-align: center;
            margin: 0 auto;
        }
    </style>
</head>
<body>



    <h3 class="slip-number">Slip Number :<?php echo $payment_slip->slip_number ?></h3>
<div class="payment-details">
    <table class="payment-body">
        <tr>
            <td>Name :</td>
            <td> <?php echo $participants->participant_name?></td>
        </tr>
        <tr>
            <td> Phone :</td>
            <td><?php echo $participants->phone_number?></td>
        </tr>
        <tr>
            <td> Email :</td>
            <td><?php echo $participants->email?></td>
        </tr>
        <tr>
            <td> Address : </td>
            <td><?php echo $participants->address?></td>
        </tr>
        <tr>
            <td>Total Seat :</td>
            <td><?php echo $booking_info->subtotal_seat ?></td>
        </tr>
        <tr>
            <td>Total Amount :</td>
            <td><?php echo $booking_info->subtotal_amount ?></td>
        </tr>
    </table>

</div>

    <script>
        window.print();
    </script>
</body>
</html>